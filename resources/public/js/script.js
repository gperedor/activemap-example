$(document).ready(function () {

    $('.spinner .btn:first-of-type').on('click', function() {
    var spinner = $(this).parent().parent().find('input');
        spinner.val(parseInt(spinner.val(), 10) + 1);
    });
    
    $('.spinner .btn:last-of-type').on('click', function() {
        var spinner = $(this).parent().parent().find('input');
        spinner.val(parseInt(spinner.val(), 10) - 1);
    });
    // $('.spinner').spinedit();
});

function fillModal(divId, url) {
    contentsContainer = $("#" + divId + " :nth-child(1)").children(".modal-body")[0];
    $.get(url, function(data) { contentsContainer.innerHTML = data; });

    $("#" + divId).modal("show");
}
