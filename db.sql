--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: manufacturers; Type: TABLE; Schema: public; Owner: ferretero; Tablespace: 
--

CREATE TABLE manufacturers (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.manufacturers OWNER TO ferretero;

--
-- Name: manufacturer_id_seq; Type: SEQUENCE; Schema: public; Owner: ferretero
--

CREATE SEQUENCE manufacturer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manufacturer_id_seq OWNER TO ferretero;

--
-- Name: manufacturer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ferretero
--

ALTER SEQUENCE manufacturer_id_seq OWNED BY manufacturers.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: ferretero; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    name character varying(255),
    manufacturer_id integer,
    stock integer,
    product_code character varying(255),
    price integer NOT NULL
);


ALTER TABLE public.products OWNER TO ferretero;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: ferretero
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO ferretero;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ferretero
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: sales; Type: TABLE; Schema: public; Owner: ferretero; Tablespace: 
--

CREATE TABLE sales (
    id integer NOT NULL,
    sale_time timestamp without time zone,
    total integer
);


ALTER TABLE public.sales OWNER TO ferretero;

--
-- Name: sale_id_seq; Type: SEQUENCE; Schema: public; Owner: ferretero
--

CREATE SEQUENCE sale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sale_id_seq OWNER TO ferretero;

--
-- Name: sale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ferretero
--

ALTER SEQUENCE sale_id_seq OWNED BY sales.id;


--
-- Name: sales_products; Type: TABLE; Schema: public; Owner: ferretero; Tablespace: 
--

CREATE TABLE sales_products (
    sale_id integer,
    product_id integer
);


ALTER TABLE public.sales_products OWNER TO ferretero;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ferretero
--

ALTER TABLE ONLY manufacturers ALTER COLUMN id SET DEFAULT nextval('manufacturer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ferretero
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ferretero
--

ALTER TABLE ONLY sales ALTER COLUMN id SET DEFAULT nextval('sale_id_seq'::regclass);


--
-- Name: manufacturer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ferretero
--

SELECT pg_catalog.setval('manufacturer_id_seq', 8, true);


--
-- Data for Name: manufacturers; Type: TABLE DATA; Schema: public; Owner: ferretero
--

COPY manufacturers (id, name) FROM stdin;
1	Redline
4	Tornado
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: ferretero
--

COPY products (id, name, manufacturer_id, stock, product_code, price) FROM stdin;
1	Diablito 15''	1	0	RED001	6
\.


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ferretero
--

SELECT pg_catalog.setval('products_id_seq', 28, true);


--
-- Name: sale_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ferretero
--

SELECT pg_catalog.setval('sale_id_seq', 1, false);


--
-- Data for Name: sales; Type: TABLE DATA; Schema: public; Owner: ferretero
--

COPY sales (id, sale_time, total) FROM stdin;
\.


--
-- Data for Name: sales_products; Type: TABLE DATA; Schema: public; Owner: ferretero
--

COPY sales_products (sale_id, product_id) FROM stdin;
\.


--
-- Name: manufacturers_pkey; Type: CONSTRAINT; Schema: public; Owner: ferretero; Tablespace: 
--

ALTER TABLE ONLY manufacturers
    ADD CONSTRAINT manufacturers_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: ferretero; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: sales_pkey; Type: CONSTRAINT; Schema: public; Owner: ferretero; Tablespace: 
--

ALTER TABLE ONLY sales
    ADD CONSTRAINT sales_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

--REVOKE ALL ON SCHEMA public FROM PUBLIC;
--REVOKE ALL ON SCHEMA public FROM postgres;
--GRANT ALL ON SCHEMA public TO postgres;
--GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

