(ns activemap-example.util)

(defmacro dbg[x] `(let [x# ~x] (println "dbg:" '~x "=" x#) x#))

(defn try-coerce
  [coerced errors k v validation error]
  (try [(assoc coerced k (validation v)) errors]
       (catch Exception e
         [coerced (assoc errors k error)])))

(defn coerce-integer
  [coerced errors k v]
  (try-coerce coerced
              errors
              k
              v
              (fn [s] (Integer/parseInt s))
              :numeric))


(defn not-null
  [coerced errors k v]
  (try-coerce coerced
              errors
              k
              v
              (fn [s] (if (empty? s)
                        (throw (NullPointerException.))
                        s))
              :null))
