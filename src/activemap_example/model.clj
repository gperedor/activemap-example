(ns activemap-example.model
  (:use activemap.meta.ddl))

(declare product manufacturer sale)

(defentity sale
  {}
  (attribute :id {:auto-inc true :pk true :data-type :integer})
  (collection-through :items product))

(defentity product
  {}
  (attribute :id {:auto-inc true :data-type :integer :pk true})
  (attribute :name {:not-null true})
  (attribute :stock {:data-type :integer})
  (attribute :product-code {:not-null true})
  (attribute :price {:not-null true :data-type :integer})
  (reference :manufacturer manufacturer))

(defentity manufacturer
  {}
  (attribute :id {:data-type :integer :auto-inc true :pk true})
  (attribute :name {:not-null true})
  (collection :products product :manufacturer))
