(ns activemap-example.models.product
  (:require [activemap.meta.types :as types]
            [activemap.core :as activemap]
            [activemap-example.model :as model]))

(defn coerce-product
  [params]
  (update-in  (types/rmap-coerce model/product params) [0 :product-code]
              (fn [code] (and code (.toUpperCase code)))))

