(ns activemap-example.views.product
  (:use activemap-example.config
        activemap-example.view)
  (:require [clostache.parser :as mustache]
            [hiccup.core :as hiccup]
            [hiccup.form :as form]))

(defn error-codes
  [field code]
  (case code
    :null "Campo requerido"
    :numeric "Por favor ingrese un número"
    nil ""))

(defn render-products
  [products & [[flash-type flash-message]]]
  (let [sorted-products (sort (fn [a b]
                                (compare (:product-code a)
                                         (:product-code b)))
                              products)
        view (map (fn [product]
                    (assoc (into {} product) :manufacturer
                           {:name (:name @(:manufacturer product))
                            :url (str "/manufacturers/"
                                      (:id @(:manufacturer product))
                                      "/edit")}))
                  sorted-products)
        flash-contents (if (not-empty flash-message)
                         (render-flash flash-type flash-message))]
    (mustache/render-resource "templates/products.mustache"
                            {:flash (hiccup/html flash-contents)
                             :contents view})))

(defn product-form
  [product manufacturers method button-label errors]
  (let [manufacturers (map (fn [f]
                             [(:name f) (:id f)])
                           manufacturers)
        render (fn [key field]
                 (render-field error-codes key field errors))]
    (form/form-to {:role "form"
                   :class "form-horizontal"}
                  method
                  (form/hidden-field :id (:id product))

                  (horizontal-wrapper
                   (form/label :code "Codigo:")
                   (render
                    :product-code
                    (form/text-field {:class "form-control"} :product-code (:product-code product))))


                  (horizontal-wrapper (form/label :name "Nombre:")
                                      (render :name (form/text-field {:class "form-control"} :name (:name product))))

                  (horizontal-wrapper (form/label "manufacturer[id]" "Fabricante:")
                                      (render "manufacturer[id]"
                                              (form/drop-down {:class "form-control"} "manufacturer[id]" manufacturers)))

                  (horizontal-wrapper (form/label :price "Precio:")
                                      (render :price
                                              (spinner :price (:price product))))

                  (horizontal-wrapper (form/label :stock "Stock:")
                                      (render :stock
                                              (spinner :stock (:stock product))))

                  [:div {:class "pull-right"}
                   [:button {:type "submit" :class "btn btn-default"} button-label]])))

(defn view-product
  [product]
  (list [:div.form-horizontal
         (horizontal-wrapper
          [:span {:class "fake-label"} "Codigo:"]
          [:span.field-data (:product-code product)])

         (horizontal-wrapper
          [:span {:class "fake-label"} "Nombre:"]
          [:span.field-data (:name product)])

         (horizontal-wrapper
          [:span {:class "fake-label"} "Fabricante:"]
          [:span.field-data (:name @(:manufacturer product))])

         (horizontal-wrapper
          [:span {:class "fake-label"} "Precio:"]
          [:span.field-data (:price product)])
         (horizontal-wrapper
          [:span {:class "fake-label"} "Stock:"]
          [:span.field-data (:stock product)])
        ]))

(defn new-product
  ([manufacturers]
     (new-product manufacturers nil nil))
  ([manufacturers current errors]
     (list [:h1 "Nuevo producto"]
           (product-form current manufacturers [:post "/products"] "Ingresar" errors))))

(defn edit-product
  ([product manufacturers]
     (edit-product product manufacturers nil))
  ([product manufacturers errors  & [[flash-type flash-message]]]
     (list [:h1 "Editar producto"]
           (when-not (nil? flash-message)
             (hiccup/html (render-flash flash-type flash-message)))
           (product-form product manufacturers
                         [:post (str "/products/" (:id product))]
                         "Actualizar"
                         errors))))
