(ns activemap-example.views.manufacturer
  (:use activemap-example.config
        activemap-example.view
        activemap.meta.types)
  (:require [clostache.parser :as stache]
            [hiccup.core :as hiccup]
            [hiccup.util :as hutil]
            [hiccup.form :as form]))

(defn error-codes
  [field code]
  (case code
    :null "Campo requerido"
    nil ""))

(defn render-manufacturers
  [manufacturers & [[flash-type flash-message]]]
  (let [view (sort (fn [a b]
                     (compare (:name a) (:name b)))
                   manufacturers)
        flash-contents (if (not-empty flash-message)
                         (render-flash flash-type flash-message))]
    (stache/render-resource "templates/manufacturers.mustache"
                            {:flash (hiccup/html flash-contents)
                             :contents view})))

(defn manufacturer-form
  [manufacturer method button-label errors]
  (let [product-names (and (= (type manufacturer) activemap.meta.types.RMap)
                           (map (fn [f]
                                  [(:name f) (:id f)])
                                @(or (:products manufacturer)
                                     (delay '()))))
        render (fn [key field]
                 (render-field error-codes key field errors))]
    (form/form-to {:role "form"
                   :class "form-horizontal"}
                  method
                  (form/hidden-field :id (:id manufacturer))

                  (horizontal-wrapper (form/label :name "Nombre:")
                                      (render :name (form/text-field {:class "form-control"} :name (:name manufacturer))))

                  (when (not-empty product-names)
                    [:div [:h2 "Productos"]
                     [:ul (map (fn [[name id]]
                                 [:li [:a {:href (hutil/url "/products/" id "/edit")}
                                    name]])
                               product-names)]])
                  [:div {:class "pull-right"}
                   [:button {:type "submit" :class "btn btn-default"} button-label]])))

(defn new-manufacturer
  ([]
     (new-manufacturer nil nil))
  ([current errors & [[flash-type flash-message]]]
     (list [:h1 "Nuevo fabricante"]
           (manufacturer-form current
                              [:post "/manufacturers"]
                              "Ingresar"
                              errors))))

(defn edit-manufacturer
  ([manufacturer]
     (edit-manufacturer manufacturer nil))
  ([manufacturer errors & [[flash-type flash-message]]]
     (list [:h1 "Editar fabricante"]
           (when-not (nil? flash-message)
             (hiccup/html (render-flash flash-type flash-message)))
           (manufacturer-form manufacturer
                              [:post (str "/manufacturers/" (:id manufacturer))]
                              "Actualizar"
                              errors))))

(defn view-manufacturer
  [manufacturer]
  (let [product-names (and (= (type manufacturer) activemap.meta.types.RMap)
                           (map (fn [f]
                                  [(:name f) (:id f)])
                                @(or (:products manufacturer)
                                     (delay '()))))]
    (list [:div.form-horizontal
           (horizontal-wrapper
            [:span {:class "fake-label"} "Nombre:"]
            [:span.field-data (:name manufacturer)])
           
           (when (not-empty product-names)
             [:div [:h2 "Productos"]
              [:ul (map (fn [[name id]]
                          [:li [:a {:href (hutil/url "/products/" id "/edit")}
                                name]])
                        product-names)]])])))
