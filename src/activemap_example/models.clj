(ns activemap-example.model
  (:use activemap.meta.ddl))

(declare product manufacturer sale)

(defentity sale
  {}
  (attribute :id {:auto-inc true :pk true :data-type :integer})
  (collection-through :items product))

(defentity product
  {}
  (attribute :id {:auto-inc true :data-type :integer :pk true})
  (attribute :name)
  (attribute :stock {:data-type :integer})
  (attribute :product-code)
  (reference :manufacturer manufacturer {:not-null true}))

(defentity manufacturer
  {}
  (attribute :id {:data-type :integer :auto-inc true :pk true})
  (attribute :name)
  (collection :products product :manufacturer))
