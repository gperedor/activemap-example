(ns activemap-example.handlers.product
  (:use activemap.core
        activemap-example.util
        activemap.meta.types
        activemap-example.models.product)
  (:require [activemap-example.model :as model]
            [activemap-example.views.product :as products-view]
            [activemap-example.view :as view]
            [hiccup.core :as hiccup]
            [ring.util.response :as ring]
            [clojureql.core :as ql]))

(defn view-product
  [id]
  (hiccup/html (products-view/view-product
                (first @(select model/product
                                {:id (Integer/parseInt id)})))))

(defn render-products
  [request]
  (println (:flash (:session request)))
  (view/render-base
   {:title "Productos"
    :body (products-view/render-products @(select model/product) (:flash request))
    :session {}}))

(defn- new-product-helper
  [params errors]
  (view/render-base {:title "Nuevo producto"
                     :body (hiccup/html
                            (products-view/new-product
                             @(select model/manufacturer)
                             params
                             errors))}))

(defn- edit-product-helper
  [record-map errors & [[flash-type flash-message]]]
  (view/render-base
   {:title "Editar producto"
    :body (hiccup/html 
           (products-view/edit-product record-map
                                       @(select model/manufacturer)
                                       errors
                                       [flash-type flash-type]))}))
(defn new-product
  []
  (view/render-base
   {:title "Nuevo producto"
    :body (hiccup/html (products-view/new-product @(select model/manufacturer)))}))

(defn create-product
  [params]
  (let [[coerced error-fields] (coerce-product params)
        invalid-fields (merge error-fields
                              (invalid? (hash-map->record-map model/product
                                                              coerced)))]
    (if (not-empty (dissoc invalid-fields :id))
      (new-product-helper params (dissoc invalid-fields :id))
      (let [manufacturer (first @(select model/manufacturer (:manufacturer coerced)))
            product (hash-map->record-map 
                     model/product
                     (assoc coerced :manufacturer manufacturer))]
        
        (try
          (insert! product)
          (merge (ring/redirect "/products")
                 {:flash [:success "Producto ingresado exitosamente"]})
          (catch Exception e
            (.printStackTrace e)
            {:status 200
             :body  (new-product-helper params nil [:danger "Ha ocurrido un error"])}))))))

(defn update-product
  [params]
  (println params)
  (let [[coerced error-fields] (coerce-product params)
        rmap (hash-map->record-map model/product coerced)
        invalid-fields (merge error-fields
                              (invalid? rmap))]
    (if (empty? invalid-fields)
      (if-not (zero? (update! rmap))
         (merge (ring/redirect "/products")
                {:flash [:success "Producto actualizado exitosamente"]})
         {:status 200
          :body (edit-product-helper rmap nil [:danger "Producto ya no existe"])})
    
      {:status 200
       :body (edit-product-helper rmap invalid-fields nil)})))

(defn delete-product
  [id]
  (try
    (ql/disj! (ql/table (:table-name model/product))
              (ql/where (= :id (Integer/parseInt id))))
    (merge (ring/redirect "/products")
           {:flash [:success "Producto eliminado"]})
    
    (catch Exception e
      (.printStackTrace e)
          (merge (ring/redirect "/products")
                 {:flash [:danger "Ha ocurrido un error"]}))))

(defn edit-product
  [id]
  (edit-product-helper (first @(select model/product
                                       {:id (Integer/parseInt id)}))
                       nil))
