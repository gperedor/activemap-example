(ns activemap-example.handlers.manufacturer
  (:use activemap.core
        activemap-example.util
        activemap.meta.types)
  (:require [activemap-example.model :as model]
            [activemap-example.views.manufacturer :as manufacturers-view]
            [activemap-example.view :as view]
            [hiccup.core :as hiccup]
            [hiccup.util :as hutil]
            [ring.util.response :as ring]
            [clojureql.core :as ql]
            [clojure.java.jdbc :as sql]))



(defn render-manufacturers
  [request]
  (view/render-base
   {:title "Fabricantes"
    :body (manufacturers-view/render-manufacturers @(select model/manufacturer) (:flash request))
    :session {}}))

(defn new-manufacturer
  []
  (view/render-base
   {:title "Nuevo fabricante"
    :body (hiccup/html (manufacturers-view/new-manufacturer))}))

(defn create-manufacturer
  [params]
  (let [[coerced error-fields] (rmap-coerce model/manufacturer params)
        invalid-fields (merge error-fields
                              (invalid? (hash-map->record-map model/manufacturer
                                                              coerced)))]
    (if (not-empty (dissoc invalid-fields :id))
      (ring/response
       (view/render-base
        {:title "Nuevo fabricante"
         :body (hiccup/html (manufacturers-view/new-manufacturer params
                                                                 invalid-fields))}))
      (try
        (insert! (hash-map->record-map model/manufacturer coerced))
        (merge (ring/redirect "/manufacturers")
               {:flash [:success "Fabricante ingresado exitosamente"]})
        (catch Exception e
          (.printStackTrace e)
          (ring/response
           (view/render-base
            {:title "Nuevo fabricante"
             :body (hiccup/html (manufacturers-view/new-manufacturer params
                                                                     nil
                                                                     [:danger "Ha ocurrido un error"]))})))))))

(defn delete-manufacturer
  [id]
  (sql/transaction 
   (try 
     (let [manufacturer (first @(select model/manufacturer
                                        {:id (Integer/parseInt id)}))]
       
       (delete! manufacturer)
       (doseq [p @(:products manufacturer)]
         (delete! p))
       (merge (ring/redirect "/manufacturers")
              {:flash [:success "Fabricante eliminado"]}))
     (catch Exception e
       (.printStackTrace e)
       (sql/set-rollback-only)
       (merge (ring/redirect "/manufacturers")
              {:flash [:danger "Ha ocurrido un error"]})))))

(defn update-manufacturer
  [params]
  (let [[coerced error-fields] (rmap-coerce model/manufacturer (dbg params))
        manufacturer (hash-map->record-map model/manufacturer coerced)
        invalid-fields (merge error-fields (invalid? manufacturer))]
    (if (empty? invalid-fields)
      (try
        (update! manufacturer)
        (merge (ring/redirect "/manufacturers")
               {:flash [:success "Fabricante actualizado"]})
        (catch Exception e
          (.printStackTrace e)
          (view/render-base
           {:title "Editar fabricante"
            :body (hiccup/html (manufacturers-view/edit-manufacturer
                                manufacturer
                                nil
                                [:danger "Ha ocurrido un error"]))})))
      (view/render-base
       {:title "Editar fabricante"
        :body (hiccup/html (manufacturers-view/edit-manufacturer
                            manufacturer
                            invalid-fields))}))))

(defn edit-manufacturer
  [id]
  (view/render-base
   {:title "Editar fabricante"
    :body (hiccup/html (manufacturers-view/edit-manufacturer
                        (first @ (select model/manufacturer
                                        {:id (Integer/parseInt id)}))))}))

(defn view-manufacturer
  [id]
  (hiccup/html 
   (manufacturers-view/view-manufacturer (first @(select model/manufacturer
                                                         {:id (Integer/parseInt id)})))))
