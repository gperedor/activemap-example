(ns activemap-example.config
  (:require [clojure.java.io :as io]
            clojure.edn))

(def config (clojure.edn/read-string (slurp (io/resource "config.edn"))))
