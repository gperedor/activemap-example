(ns activemap-example.view
  (:use activemap-example.util)
  (:require [clostache.parser :as mustache]
            [hiccup.core :as hiccup]
            [hiccup.form :as form]))

(defn render-base
  [contents]
  (mustache/render-resource "templates/base.mustache"
                          contents))

(defn render-flash
  [flash-type flash-message]
  [:div {:class (str "alert fade in " "bg-" (name flash-type))}
   [:button {:class "close" :data-dismiss "alert"} "&times;"]
   flash-message])

(defn render-field
  [codes property field errors]
  [:div field
   [:span (if (contains? errors property)
            {:class "error-label"}
            {:style "display:none"})
    (codes property (dbg (get (dbg errors) (dbg property))))]])

(defn spinner
  [name value]
  [:div {:class "input-group spinner"}
   [:input {:type "text" :class "form-control" :id name :name name
            :value (or (and (coll? value) (not-empty value))
                       0)}]

   [:div  {:class "input-group-btn-vertical"}
    [:button {:class "btn btn-default btn-xs" :tabindex "-1" :type "button"} [:i {:class "glyphicon glyphicon-chevron-up"}]]
    [:button {:class "btn btn-default btn-xs" :tabindex "-1" :type "button"} [:i {:class "glyphicon glyphicon-chevron-down"}]]]])

(defn horizontal-wrapper
  [label field]
  [:div {:class "form-group"}
   (update-in label [1 :class] #(str "col-sm-2 control-label " %))
   [:div {:class "col-sm-10"} field]])


;; <div class="container">
;;   <div class="page-header"><h1>Bootstrap 3 input-spinner</h1></div>  
;;   <div class="input-group spinner">
;;     <input type="text" class="form-control" value="42">
;;     <div class="input-group-btn-vertical">
;;       <button class="btn btn-default"><i class="fa fa-caret-up"></i></button>
;;       <button class="btn btn-default"><i class="fa fa-caret-down"></i></button>
;;     </div>
;;   </div>
;; </div>
