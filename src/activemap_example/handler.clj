(ns activemap-example.handler
    (:use [ring.middleware.json :only (wrap-json-response wrap-json-params)]
          ring.adapter.jetty
          [ring.util.response :only (response redirect)]
          [ring.middleware reload
                           params
                           keyword-params
                           nested-params
                           multipart-params
                           session
                           flash]
          compojure.core
          compojure.handler
          activemap.core
          activemap-example.config)
    (:require [clojure.java.jdbc :as sql]
              [compojure.route :as route]
              clojure.edn
              [clojure.java.io :as io]
              [activemap-example.model :as models]
              [activemap-example.view :as view]
              [activemap-example.handlers.product :as product-handlers]
              [activemap-example.handlers.manufacturer :as manufacturer-handlers]))

(def credentials
  (:db config))

(defn wrap-db-connection
  [handler]
  (fn [request]
    (sql/with-connection credentials
      (handler request))))

(defroutes main-routes
  (GET  "/" [] (redirect "/products"))

  (GET  "/products" request (product-handlers/render-products request))
  (GET  "/products/new" [] (product-handlers/new-product))
  (POST "/products" {params :params} (product-handlers/create-product params))
  (POST "/products/:id/delete" [id] (product-handlers/delete-product id))
  (GET  "/products/:id/edit" [id] (product-handlers/edit-product id))
  (POST "/products/:id" {params :params}  (product-handlers/update-product params))
  (GET "/products/:id" [id] (product-handlers/view-product id))

  (GET "/manufacturers" request (manufacturer-handlers/render-manufacturers request))
  (GET "/manufacturers/new" [] (manufacturer-handlers/new-manufacturer))
  (GET "/manufacturers/:id/edit" [id] (manufacturer-handlers/edit-manufacturer id))
  (POST "/manufacturers" {params :params} params (manufacturer-handlers/create-manufacturer params))
  (POST "/manufacturers/:id/delete" [id] (manufacturer-handlers/delete-manufacturer id))
  (POST "/manufacturers/:id" {params :params} (manufacturer-handlers/update-manufacturer params))
  (GET "/manufacturers/:id" [id] (manufacturer-handlers/view-manufacturer id))

  (route/resources "/"))

(defn wrap-nil-params
  [handler]
  (fn [request]
    (-> (update-in request [:params]
                   (fn [ps]
                     (->> ps
                          (map (fn [[k v]] (if (= v "")
                                             [k nil]
                                             [k v])))
                          (into {}))))
        (handler))))

(defn wrap-dbg
  [handler]
  (fn [request]
    (println "Map body" request)
    (handler request)))

(def app
  (-> main-routes
      wrap-db-connection
      wrap-keyword-params
      wrap-nested-params
      wrap-nil-params
      wrap-params
      wrap-flash
      wrap-session))

(defn boot
  []
  (run-jetty #'app {:port 8080}))
