(defproject activemap-example "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]

                 ;; DB
                 [activemap "0.1.0-SNAPSHOT"]
                 [clojureql "1.0.4"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [org.postgresql/postgresql "9.3-1100-jdbc41"]
                 
                 ;; Webz
                 [hiccup "1.0.5"]
                 [compojure "1.1.7"]
                 [ring "1.2.2"]
                 [ring/ring-json "0.2.0"]
                 [de.ubercode.clostache/clostache "1.4.0"]]

  :main activemap-example.handler/boot
  :plugins [[lein-ring "0.8.10"]]
  :ring {:handler activemap-example.handler/app})
